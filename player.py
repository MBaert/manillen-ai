import random

class RandomPlayer():
    def __init__(self):
        self.name = 'Random player'

    def state_update(self, state):
        pass

    def get_action(self, state):
        self.state_update(state)
        hand_cards = state['hand']

        return random.choice(hand_cards)

class AIPlayer():
    def __init__(self):
        self.name = 'AI player'

    def load_config(self, config):
        pass

    def state_update(self, state):
        pass

    def get_action(self, state):
        self.state_update(state)
        hand_cards = state['hand']

        return random.choice(hand_cards)


class HumanPlayer():
    def __init__(self):
        self.name = 'AI player'

    def state_update(self, state):
        pass

    def get_action(self, state):
        self.state_update(state)
        # TODO some UI to input card