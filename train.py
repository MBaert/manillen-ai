from gameEngine import Manillen
from player import RandomPlayer, AIPlayer
import random
import numpy as np

N_POPULATION = 4*250
N_GENERATIONS = 1

def init_population():
    players = [AIPlayer() for _ in range(N_POPULATION)]
    return players


def run_training():
    players = init_population()
    scores = []
    
    for gen in range(N_GENERATIONS):
        random.shuffle(players)

        # run all the games
        for table in range(len(players)//4): # one table
            players_table = players[table*4:table*4+4]

            for spel in range(3): # iedereen speelt met iedereen
                game = Manillen()
                players_spel = [players_table[0], players_table[1:][spel%3], players_table[1:][(spel+1)%3], players_table[1:][(spel+2)%3]]
                game.load_players(players_spel)

                for start_player in range(0,12): # 12 spelletjes
                    game.run_round(start_player%4)
            
            scores += game.get_score()

        # kill half of population
        fitness = [score/sum(scores) for score in scores]
        players = np.random.choice(players, p=fitness, size = N_POPULATION//2, replace = False)

        # mutate

        




