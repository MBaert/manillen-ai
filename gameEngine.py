import random

class Manillen():
    def __init__(self):
        self.name = 'Manillen'
        self.players = []
        self.hand_cards = [[] for _ in range(4)]
        self.score = [0, 0, 0, 0]
        self.soorten = ['C', 'D', 'H', 'S', 'zonder']
        self.nummers = ['7','8','9','J','Q','K','A','10']
        self.card_score = [0,0,0,1,2,3,4,5]
        self.print_stuff = False

    def load_players(self, players):
        self.players = players

    def get_score(self):
        return self.score

    def calc_score_points(self, points_round):
        return [max(points_round[i] + points_round[(i+2)%4] -30,0) for i in range(4)]

    def string_card(self, card):
        return '{}_{}'.format(self.soorten[card[0]], self.nummers[card[1]])

    def run_round(self, start_player):
        self.dist_cards()
        troef = random.randint(0,4) # TEMP
        if self.print_stuff:
            print('troef: {}'.format(self.soorten[troef]))
        points_round = [0,0,0,0]
        for _ in range(8):
            start_player, points_slag = self.run_slag(start_player, troef)
            points_round[start_player] += points_slag

        score_round = self.calc_score_points(points_round)
        self.score = [self.score[i] + score_round[i] for i in range(4)]

    def dist_cards(self):
        deck_cards = [(s, n) for s in range(4) for n in range(8)]
        random.shuffle(deck_cards)
        for i in range(4):
            self.hand_cards[i] = deck_cards[i*8:i*8+8]
        
    def run_slag(self, start_player, troef):
        table_cards = [0 for i in range(4)]
        for i in range(start_player, start_player + 4):
            player_index = i%4
            state = self.gen_state_player(player_index, table_cards, troef)
            card = self.players[player_index].get_action(state)

            while not self.check_valid_move(player_index, card, start_player, table_cards, troef):
                card = self.players[player_index].get_action(state)

            table_cards = self.play_card(player_index, card, table_cards)

        win_player, points_slag = self.calc_points(start_player, table_cards, troef)

        # display
        if self.print_stuff:
            print_string = 'start player: {} \t'.format(start_player+1)
            for card in table_cards:
                print_string += self.string_card(card)
                print_string += '\t'
            print_string += 'win player: {}'.format(win_player+1)
            print(print_string)

        return win_player, points_slag

    def check_valid_move(self, player_index, card_play, start_player, table_cards, troef):
        
        if player_index == start_player:
            return True

        hand_cards = self.hand_cards[player_index]
        startSoort = table_cards[start_player][0]
        maat_player_index = (player_index + 2)%4

        # highest card berekening
        highest_card, highest_card_player = self.calc_highest_card(start_player, table_cards, troef)

        # volgen als je kunt
        kunnen_volgen = any([card[0] == startSoort for card in hand_cards])
        if not card_play[0] == startSoort and kunnen_volgen:
            return False

        # als volgen, hoger als je kunt, tenzij maat
        if kunnen_volgen:
            if card_play[1] < highest_card[1] and (not highest_card_player == maat_player_index):
                cards_soort = [card for card in hand_cards if card[0] == startSoort]
                if any([card[1] > highest_card[1] for card in cards_soort]):
                    return False

        # zoniet, kopen als je kunt (tenzij maat)
        # als kopen, hoger als je kunt (tenzij maat), onder kopen hoeft niet
        if (not troef == 4) and (not kunnen_volgen) and (not highest_card_player == maat_player_index):
            troef_cards = [card for card in hand_cards if card[0] == troef]
            if (not card_play[0] == troef) and troef_cards:
                if highest_card[0] == troef:
                    if any([card[1] > highest_card[1] for card in troef_cards]):
                        return False
                else:
                    return False

        return True

    def play_card(self, player_index, card, table_cards):
        self.hand_cards[player_index].pop(self.hand_cards[player_index].index(card))
        table_cards[player_index] = card
        return table_cards

    def calc_points(self, start_player, table_cards, troef):
        highest_card, highest_card_player = self.calc_highest_card(start_player, table_cards, troef)
        points_slag = sum([self.card_score[card[1]] for card in table_cards])
        win_player = highest_card_player
        return win_player, points_slag

    def gen_state_player(self, player_index, table_cards, troef):
        state = {}
        state['hand'] = self.hand_cards[player_index]
        state['table'] = table_cards
        state['troef'] = troef

        return state

    def calc_highest_card(self, start_player, table_cards, troef):
        startSoort = table_cards[start_player][0]
        highest_card = table_cards[start_player]
        highest_card_player = start_player
        for i in range(1,4):
            highest = False
            card = table_cards[(start_player + i)%4]
            if card == 0: # nog geen kaart gelegd
                break
            if card[0] == startSoort and highest_card[0] == startSoort:
                if card[1] > highest_card[1]:
                    highest = True
            elif card[0] == troef:
                if highest_card[0] == troef:
                    if card[1] > highest_card[1]:
                        highest = True
                else:
                    highest = True

            if highest:
                highest_card = card
                highest_card_player = (start_player + i)%4

        return highest_card, highest_card_player


    def render(self):
        pass


    


    
